# electricityMap challenge

This repository contains the code looking at the electricityMap challenge.

## Content of the repository
The repository contains the following files:
- **README.md** : this current file, describing the content
- **report.ipynb** : this is the report, that walks you through the steps of the code usage and presents the solution to the challenge
- Python files : definition of the methods used in the reporting 
  - **database_io.py** for database creation and data loading from Energinet/ENTSO-E
  - **emission_intensity.py** for the methods related to computing emission intensity
  - **visualisation.py** for the methods used in data visualisation
- **requirements.txt** : the classic requirements file to setup your Python environment (make sure to run this in **Python>=3.9**)
- Configuration and license files (.gitignore, .gitlab-ci.yml, pre-commit-config.yaml, LICENSE) which do not require specific attention.


The code is tested on Ubuntu Linux, but should in principle run just as good on other platforms.
