"""
This module contains the code for creating the database, and loading data into it.
"""
import json
import logging
import os
import sqlite3
import time
from datetime import datetime
from typing import Optional, Tuple

import numpy as np
import pandas as pd
import pytz
import requests

# Parameters for execution
DATABASE_FILE = "data/database.db"
START_DATE = datetime(2020, 1, 1)
END_DATE = datetime(2021, 1, 1)

# Development mode is meant to be used for caching of API calls results in order to save bandwidth
DEVELOPMENT_MODE = True


def __database_cursor() -> Tuple[sqlite3.Connection, sqlite3.Cursor]:
    """
    This is just a function to avoid code repetition
    :return: Connection, Cursor
    """
    db_conn = sqlite3.connect(DATABASE_FILE)
    c = db_conn.cursor()
    return db_conn, c


def __write_to_database(df: pd.DataFrame, table: str) -> None:
    """
    Writes a timeseries dataframe to the database

    :param df: dataframe to write to the database (columns: start_time, value, value_type, area)
    :param table: target table to which to write (timeseries)
    :return: /
    """
    if table not in ["timeseries"]:
        raise ValueError(f"Invalid target table ({table})")
    db_conn, __ = __database_cursor()
    try:
        df.to_sql(table, db_conn, if_exists="append", index=False)
    finally:
        db_conn.close()


def __read_from_database(query: str) -> pd.DataFrame:
    """
    Method to read from the database

    :param query: query to run
    :return: result of the query in dataframe format
    """
    db_conn, __ = __database_cursor()
    try:
        out = pd.read_sql(query, db_conn)
    finally:
        db_conn.close()
    return out


def __import_production_data(start: datetime, end: datetime, area: str = "DK1") -> None:
    """
    Imports the production data from the ENTSOE API

    :param start: start of the period to load
    :param end: end of the period to load
    :param area: area to load (DK1 or DK2)
    :return: /
    """

    # The pickle caching of the request is just there to allow quick reloading in the database without making a heavy call
    # (it was only used for development purposes, but I thought I'd leave it here for now)
    cache_id = f"data/entsoe_prod_{area.lower()}_{start}_{end}.pkl".replace(
        ":", ""
    ).replace(" ", "_")
    if DEVELOPMENT_MODE and os.path.exists(cache_id):
        df = pd.read_pickle(cache_id)
    else:
        df = fetch_production_data_entsoe_denmark(area=area, start=start, end=end)
        if DEVELOPMENT_MODE:
            df.to_pickle(cache_id)

    # Reordering and renaming the dataframe to match database columns and writing them
    for k in df.keys():
        if k not in ["t", "geothermal"]:
            df_k = df[[k, "t"]].rename(columns={k: "value", "t": "start_time"})
            df_k["value_type"] = k
            df_k["area"] = area.upper()
            __write_to_database(df_k, table="timeseries")


def create_database(skip_if_exists: bool = True, force_refresh: bool = False) -> bool:
    """
    This script creates the database if needed

    :param skip_if_exists: if True, skips the creation of the database if it already exists
    :param force_refresh: if True, deletes the existing database and creates a fresh new one
    :return: True if a database was created in this run, False otherwise
    """
    if os.path.exists(DATABASE_FILE):
        if force_refresh:
            os.remove(DATABASE_FILE)
        elif skip_if_exists:
            print("The database already exists - skipping creation")
            return False

    # Create the data folder if it does not exist
    if os.path.isdir("data") is False:
        os.mkdir("data")

    db_conn, c = __database_cursor()

    # Here, a single data structure for the timeseries is used
    # (with a generic format allowing to accommodate all timeseries in this use case)
    #
    # Moreover, a unique constraint is used in order to avoid duplicates
    c.execute(
        """
    CREATE TABLE timeseries (
        start_time INTEGER,
        value REAL,
        value_type TEXT,
        area TEXT,
        UNIQUE(start_time, value_type, area)
    );
    """
    )

    for area in ["DK1", "DK2"]:
        print(f"Loading area {area}:")
        try:
            print("-- Production data loading from ENTSO-E:")
            __import_production_data(START_DATE, END_DATE, area=area)
            print("    completed")
        except Exception as e:
            print(f"   error: {e}")

    db_conn.close()
    return True


def __format_df(df: pd.DataFrame, preprocess: bool = True) -> pd.DataFrame:
    """
    Formats a given dataset obtained by API calls

    :param df: dataset to format
    :param preprocess: if True, operates a pre-processing of the columns
        (with a basic 0-minimum and maximum limited to median + 4 sigma)
    :return: restructured dataset
    """
    cols = df["value_type"].unique()
    y = pd.DataFrame()
    for c in cols:
        y_c = df[df["value_type"] == c][["value", "start_time"]].rename(
            columns={"value": c}
        )
        y_c.index = pd.to_datetime(y_c["start_time"])
        y_c.drop(columns=["start_time"], inplace=True)
        if y.empty:
            y = y_c
        else:
            y = y.merge(y_c, how="outer", left_index=True, right_index=True)

    if preprocess is True:
        # As a rough starting point, production data is filtered with a minimum at zero and a 4 sigma limit on maximum
        max_df = y.median() + 4 * y.std()
        y[y < 0] = np.nan
        y[y > max_df] = np.nan

    return y


def read_timeseries(
    area: str,
    freq: Optional[str] = None,
    preprocess: bool = True,
    start: Optional[datetime] = None,
    end: Optional[datetime] = None,
) -> pd.DataFrame:
    """
    This method reads the data from the local database

    :param area: area to load from (DK1 or DK2)
    :param freq: frequency of the data to load (None to load raw data, "H" to load hourly data)
    :param preprocess: if True, carries out a rough preprocessing of the data
    :param start: start time of the data loading (None yields the start of the focus period)
    :param end: end time of the data loading (None yields the end of the focus period)
    :return: dataset for further analysis, structured as a dataframe
    """

    query = f"SELECT * FROM timeseries WHERE area='{area}'"
    if start is not None:
        query += f" AND start_time>='{start.isoformat()}'"
    if end is not None:
        query += f" AND start_time<'{end.isoformat()}'"
    df = __read_from_database(query)
    out = __format_df(df, preprocess=preprocess)
    if freq is None:
        pass
    elif freq == "H":
        out = out.resample("H").mean()
    else:
        raise ValueError(f"Illegal freq resampling ({freq})")
    return out


def fetch_production_data_entsoe_denmark(
    start: datetime, end: datetime, area: str
) -> pd.DataFrame:
    """
    Fetches production data from the ENTSO-E API

    :param start: start time of the data loading
    :param end: end time of the data loading
    :param area: area to load data for ("DK1" or "DK2")
    :return: production dataset in dataframe format
    """
    zone_key = f"DK-{area}"
    x = fetch_production(
        start, end, zone_key=zone_key, session=None, target_datetime=None
    )
    df = pd.DataFrame(x)
    df_prod = df["production"].apply(pd.Series)
    df_prod["t"] = pd.to_datetime(df["datetime"])
    return df_prod


# -----------------------------------------------------------------------------------------------------
#  The rest below is a quick reuse of electricityMap's code with a minor adaptation for start/end time

# This is just a quickfix to avoid having to link to other files of the electricityMap repository
class ParserException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__("DK parser error")


ids = {
    "real_time": "06380963-b7c6-46b7-aec5-173d15e4648b",
    "energy_bal": "02356e88-7c4e-4ee9-b896-275d217cc1b9",
}


def fetch_production(
    start: datetime,
    end: datetime,
    zone_key="DK-DK1",
    session=None,
    target_datetime=None,
    logger: logging.Logger = logging.getLogger(__name__),
) -> dict:
    """
    Queries "Electricity balance Non-Validated" from energinet api
    for Danish bidding zones
    NOTE: Missing historical wind/solar data @ 2017-08-01
    """
    r = session or requests.session()

    if zone_key not in ["DK-DK1", "DK-DK2"]:
        raise NotImplementedError(
            "fetch_production() for {} not implemented".format(zone_key)
        )

    zone = zone_key[-3:]

    start_str = start.strftime("%Y-%m-%d %H:%M")
    end_str = end.strftime("%Y-%m-%d %H:%M")

    # fetch hourly energy balance from recent hours
    sqlstr = 'SELECT "HourUTC" as timestamp, "Biomass", "Waste", \
                     "OtherRenewable", "FossilGas" as gas, "FossilHardCoal" as coal, \
                     "FossilOil" as oil, "HydroPower" as hydro, \
                     ("OffshoreWindPower"%2B"OnshoreWindPower") as wind, \
                     "SolarPower" as solar from "{0}" \
                     WHERE "PriceArea" = \'{1}\' AND \
                     "HourUTC" >= timestamp\'{2}\' AND \
                     "HourUTC" < timestamp\'{3}\' \
                     ORDER BY "HourUTC" ASC'.format(
        ids["energy_bal"], zone, start_str, end_str
    )

    url = "https://api.energidataservice.dk/datastore_search_sql?sql={}".format(sqlstr)
    response = r.get(url)

    # raise errors for responses with an error or no data
    retry_count = 0
    while response.status_code in [429, 403, 500]:
        retry_count += 1
        if retry_count > 5:
            raise Exception("Retried too many times..")
        # Wait and retry
        logger.warn("Retrying..")
        time.sleep(5 ** retry_count)
        response = r.get(url)
    if response.status_code != 200:
        j = response.json()
        if "error" in j and "info" in j["error"]:
            error = j["error"]["__type"]
            text = j["error"]["info"]["orig"]
            msg = '"{}" fetching production data for {}: {}'.format(
                error, zone_key, text
            )
        else:
            msg = "error while fetching production data for {}: {}".format(
                zone_key, json.dumps(j)
            )
        raise requests.exceptions.HTTPError(msg)
    if not response.json()["result"]["records"]:
        raise ParserException("DK.py", "API returned no data", zone_key=zone_key)

    df = pd.DataFrame(response.json()["result"]["records"])
    # index response dataframe by time
    df = df.set_index("timestamp")
    df.index = pd.DatetimeIndex(df.index)
    # drop empty rows from energy balance
    df.dropna(how="all", inplace=True)

    # Divide waste into 55% renewable and 45% non-renewable parts according to
    # https://ens.dk/sites/ens.dk/files/Statistik/int.reporting_2016.xls (visited Jan 24th, 2019)
    df["unknown"] = 0.45 * df["Waste"]  # Report fossil waste as unknown
    df["renwaste"] = 0.55 * df["Waste"]
    # Report biomass, renewable waste and other renewables (biogas etc.) as biomass
    df["biomass"] = df.filter(["Biomass", "renwaste", "OtherRenewable"]).sum(axis=1)

    fuels = ["biomass", "coal", "oil", "gas", "unknown", "hydro"]
    # Format output as a list of dictionaries
    output = []
    for dt in df.index:

        data = {
            "zoneKey": zone_key,
            "datetime": None,
            "production": {
                "biomass": 0,
                "coal": 0,
                "gas": 0,
                "hydro": None,
                "nuclear": 0,
                "oil": 0,
                "solar": None,
                "wind": None,
                "geothermal": None,
                "unknown": 0,
            },
            "storage": {},
            "source": "api.energidataservice.dk",
        }

        data["datetime"] = dt.to_pydatetime()
        data["datetime"] = data["datetime"].replace(tzinfo=pytz.utc)
        for f in ["solar", "wind"] + fuels:
            data["production"][f] = df.loc[dt, f]
        output.append(data)
    return output


def fetch_exchange(
    start: datetime,
    end: datetime,
    zone_key1="DK-DK1",
    zone_key2="DK-DK2",
    session=None,
    target_datetime=None,
    logger=logging.getLogger(__name__),
):
    """
    Fetches 5-minute frequency exchange data for Danish bidding zones
    from api.energidataservice.dk
    """
    r = session or requests.session()
    sorted_keys = "->".join(sorted([zone_key1, zone_key2]))

    # pick the correct zone to search
    if "DK1" in sorted_keys and "DK2" in sorted_keys:
        zone = "DK1"
    elif "DK1" in sorted_keys:
        zone = "DK1"
    elif "DK2" in sorted_keys:
        zone = "DK2"
    elif "DK-BHM" in sorted_keys:
        zone = "DK2"
    else:
        raise NotImplementedError(
            "Only able to fetch exchanges for Danish bidding zones"
        )

    exch_map = {
        "DE->DK-DK1": '"ExchangeGermany"',
        "DE->DK-DK2": '"ExchangeGermany"',
        "DK-DK1->DK-DK2": '"ExchangeGreatBelt"',
        "DK-DK1->NO-NO2": '"ExchangeNorway"',
        "DK-DK1->NL": '"ExchangeNetherlands"',
        "DK-DK1->SE": '"ExchangeSweden"',
        "DK-DK1->SE-SE3": '"ExchangeSweden"',
        "DK-DK1->NL": '"ExchangeNetherlands"',
        "DK-DK2->SE": '("ExchangeSweden" - "BornholmSE4")',  # Exchange from Bornholm to Sweden is included in "ExchangeSweden"
        "DK-DK2->SE-SE4": '("ExchangeSweden" - "BornholmSE4")',  # but Bornholm island is reported separately from DK-DK2 in eMap
        "DK-BHM->SE": '"BornholmSE4"',
    }
    if sorted_keys not in exch_map:
        raise NotImplementedError("Exchange {} not implemented".format(sorted_keys))

    start_str = start.strftime("%Y-%m-%d %H:%M")
    end_str = end.strftime("%Y-%m-%d %H:%M")

    # fetch real-time/5-min data
    sqlstr = 'SELECT "Minutes5UTC" as timestamp, {0} as "netFlow" \
                     from "{1}" WHERE "PriceArea" = \'{2}\' AND \
                     "Minutes5UTC" >= timestamp\'{3}\' AND \
                     "Minutes5UTC" < timestamp\'{4}\' \
                     ORDER BY "Minutes5UTC" ASC'.format(
        exch_map[sorted_keys], ids["real_time"], zone, start_str, end_str
    )

    url = "https://api.energidataservice.dk/datastore_search_sql?sql={}".format(sqlstr)
    response = r.get(url)

    # raise errors for responses with an error or no data
    retry_count = 0
    while response.status_code in [429, 403, 500]:
        retry_count += 1
        if retry_count > 5:
            raise Exception("Retried too many times..")
        # Wait and retry
        logger.warn("Retrying..")
        time.sleep(5 ** retry_count)
        response = r.get(url)
    if response.status_code != 200:
        j = response.json()
        if "error" in j and "info" in j["error"]:
            error = j["error"]["__type"]
            text = j["error"]["info"]["orig"]
            msg = '"{}" fetching exchange data for {}: {}'.format(
                error, sorted_keys, text
            )
        else:
            msg = "error while fetching exchange data for {}: {}".format(
                sorted_keys, json.dumps(j)
            )
        raise requests.exceptions.HTTPError(msg)
    if not response.json()["result"]["records"]:
        raise ParserException("DK.py", "API returned no data", zone_key=sorted_keys)

    df = pd.DataFrame(response.json()["result"]["records"])
    df = df.set_index("timestamp")
    df.index = pd.DatetimeIndex(df.index)
    # drop empty rows
    df.dropna(how="all", inplace=True)

    # all exchanges are reported as net import,
    # where as eMap expects net export from
    # the first zone in alphabetical order
    if "DE" not in sorted_keys:
        df["netFlow"] = -1 * df["netFlow"]
    # Format output
    output = []
    for dt in df.index:
        data = {
            "sortedZoneKeys": sorted_keys,
            "datetime": None,
            "netFlow": None,
            "source": "api.energidataservice.dk",
        }

        data["datetime"] = dt.to_pydatetime()
        data["datetime"] = data["datetime"].replace(tzinfo=pytz.utc)
        data["netFlow"] = df.loc[dt, "netFlow"]
        output.append(data)
    return output
