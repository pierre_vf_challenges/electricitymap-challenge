"""
This module contains the methods to create the plots and visualisations used in the report notebook.
"""

from datetime import datetime, timedelta

import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from plotneat.plotly_addons import prepare_minimalistic_show

from database_io import END_DATE, START_DATE, read_timeseries

# Choosing some reasonable colours for the plots
colors = {
    "oil": "grey",
    "gas": "brown",
    "coal": "black",
    "unknown": "purple",
    "nuclear": "red",
    "solar": "yellow",
    "wind": "blue",
    "hydro": "cyan",
    "biomass": "orange",
}


def plot_difference_dk1_dk2(
    show: bool = False, start: datetime = START_DATE, end: datetime = END_DATE
) -> go.Figure:
    """
    This method plots the difference between DK1 and DK2

    :param show: if True, displays the figure
    :param start: start of period for plotting
    :param end: end of period for plotting
    :return: resulting plotly figure
    """
    fig = make_subplots(
        2,
        1,
        shared_xaxes=True,
        shared_yaxes=True,
        subplot_titles=["Western Denmark (DK1)", "Eastern Denmark (DK2)"],
    )
    for i in [1, 2]:
        # Loading data and making a very basic preprocessing
        df_i = read_timeseries(f"DK{i}", freq="H", start=start, end=end)

        for c in colors.keys():
            fig.add_trace(
                go.Scatter(
                    x=df_i.index,
                    y=df_i[c],
                    name=c,
                    mode="lines",
                    line=dict(color=colors[c]),
                    legendgroup="Production",
                    stackgroup="one",
                ),
                i,
                1,
            )

    fig.layout["yaxis1"]["title"] = "Production [MW]"
    fig.layout["yaxis2"]["title"] = "Production [MW]"

    # Doing aesthetics cleanup on the figure
    prepare_minimalistic_show(fig)

    if show is True:
        fig.show()

    return fig


def plot_emission_intensity(df: pd.DataFrame, show: bool = False) -> go.Figure:
    """
    Method to evaluate the emission intensity given a production mix (imports/exports are neglected)

    :param df: dataframe of the production
    :param show: if True, plots the figure
    :return: plotly figure
    """

    if "average_co2_intensity" not in df.keys():
        from emission_intensity import compute_emission_intensity

        df = compute_emission_intensity(df)

    fig = go.Figure()
    fig.add_trace(
        go.Scattergl(
            x=df.index, y=df["average_co2_intensity"], name="Average carbon intensity"
        )
    )
    fig.layout["yaxis"]["title"] = "Average emission intensity [gCO2eq/kWh]"

    # Doing aesthetics cleanup on the figure
    prepare_minimalistic_show(fig)
    if show:
        fig.show()

    return fig


def __add_coloured_background_area(fig: go.Figure, xs, color: str = "green") -> None:
    """
    Adds a coloured area to the background of a plot

    :param fig: figure to augment with the background areas
    :param xs: limits of the area to plot on the x-axis
    :param color: colour to use for the background area
    :return: /
    """
    if fig.layout.yaxis.range is not None:
        ys = [0, fig.layout.yaxis.range[1]]
    else:
        ys = [0, 800]

    colored_background = [
        {
            "x0": xs[0],
            "x1": xs[1],
            "y0": ys[0],
            "y1": ys[1],
            "fillcolor": color,
            "type": "rect",
            "line": {"color": color, "dash": "solid", "width": 1},
            "opacity": 0.2,
        }
    ]
    fig["layout"]["shapes"] += tuple(colored_background)
    fig.layout.yaxis.range = ys


def intensity_traffic_light_plot(
    df: pd.DataFrame, show: bool = True, n_hours=6
) -> go.Figure:
    """
    This method plots a traffic-light-style emission intensity timeseries

    :param df: emission intensity dataset (with column "average_co2_intensity")
    :param show: if True, shows the plot
    :return: the plotly figure generated
    """
    # Adding a plot with traffic light style
    q = n_hours / len(df)
    bounds = [
        df["average_co2_intensity"].quantile(q=q),
        df["average_co2_intensity"].quantile(q=1 - q),
    ]

    fig = go.Figure()
    fig.add_trace(
        go.Scatter(x=df.index, y=df["average_co2_intensity"], line={"shape": "hv"})
    )
    for i in df.index:
        y_i = df.loc[i, "average_co2_intensity"]
        if y_i <= bounds[0]:
            color_i = "green"
        elif y_i <= bounds[1]:
            color_i = "orange"
        else:
            color_i = "red"
        __add_coloured_background_area(
            fig, xs=[i, i + timedelta(minutes=60)], color=color_i
        )
    fig["layout"]["yaxis"]["title"] = "Carbon intensity of electricity [gCO2eq/kWh]"

    if show:
        fig.show()

    return fig
